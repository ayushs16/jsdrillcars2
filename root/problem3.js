
const sortByModel = function( inventory,order = 1 ){
    const unsortedInventory = [ ... inventory ];
    
    const sortedInventory = unsortedInventory.sort(( a , b ) => {
        if( a.car_model.toLowerCase() >= b.car_model.toLowerCase() ){
            return 1 * order;
        }
        else{
            return -1 * order;
        }
    });
    return sortedInventory;
}
module.exports = { sortByModel };