const carsOlderThan = function( inventory,year ){
    const carArr = inventory.filter( ( car ) => {
        if( car.car_year < year ){
            return true;
        }
    });
    console.log(`${ carArr.length } cars were made before ${ year }`);
    return carArr;
}

module.exports = { carsOlderThan };