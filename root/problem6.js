const findByCarMake = function( inventory , carMake ){
    let arr = [ ... carMake ];

    const carsFound = inventory.filter( ( car ) => {
        if( arr.includes( car.car_make ) ){
            return true;
        }
    })
    return carsFound;
}

module.exports = { findByCarMake };