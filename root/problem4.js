
const getCarYear = function( inventory ){
    const arr = inventory.map( ( car ) => {
        return car.car_year;
    })   
    return arr;
}

module.exports = { getCarYear };