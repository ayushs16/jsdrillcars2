
const findCarById = function( inventory , givenId ){
    try {
        const resultArr = inventory.filter( ( car )=>{ 
            return ( car.id === givenId );
        });
        
        const found = resultArr[0];

        return `Car ${ givenId } is a ${ found.car_year } ${ found.car_make } ${ found.car_model }`;
    } 
    catch( error ){
        return `car with id : ${ givenId } not found`;
    }
}


module.exports = { findCarById };