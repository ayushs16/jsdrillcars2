const { inventory } = require( '../app' );
const { sortByModel } = require( '../root/problem3' );

// pass inventory and order (pass 1 for ascending or -1 for descending order) in parameters.
console.log( sortByModel( inventory , 1 ) );